package ru.inno.repos;

import org.springframework.data.repository.CrudRepository;
import ru.inno.domain.Post;

public interface PostRepo extends CrudRepository<Post, Long> {
}
