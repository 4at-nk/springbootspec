package ru.inno.repos;

import org.springframework.data.repository.CrudRepository;
import ru.inno.domain.DocTemplate;

public interface DocTemplateRepo extends CrudRepository<DocTemplate, Long> {
}
