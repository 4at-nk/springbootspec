package ru.inno.repos;

import org.springframework.data.repository.CrudRepository;
import ru.inno.domain.BPTemplate;

public interface BPTemplateRepo extends CrudRepository<BPTemplate, Long> {
}
