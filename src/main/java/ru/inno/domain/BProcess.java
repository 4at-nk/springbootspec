package ru.inno.domain;

import javax.persistence.*;

@Entity
public class BProcess {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "author_id")
    private Person author;

    @ManyToOne
    @JoinColumn(name = "bpTemplate_id")
    private BPTemplate bpTemplate;

    private String description;

    private String docLink;

    public Person getAuthor() {
        return author;
    }

    public void setAuthor(Person author) {
        this.author = author;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDocLink() {
        return docLink;
    }

    public void setDocLink(String docLink) {
        this.docLink = docLink;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BPTemplate getBpTemplate() {
        return bpTemplate;
    }

    public void setBpTemplate(BPTemplate bpTemplate) {
        this.bpTemplate = bpTemplate;
    }
}