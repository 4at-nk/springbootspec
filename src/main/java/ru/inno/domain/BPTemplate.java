package ru.inno.domain;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Entity
public class BPTemplate {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotBlank(message = "description cannot be empty")
    private String description;

    @ManyToOne
    @JoinColumn(name = "docTemplate_id")
    private DocTemplate docTemplate;

    @ElementCollection(targetClass = Post.class, fetch = FetchType.EAGER)
    @CollectionTable(name = "bpTemp_scenario", joinColumns = @JoinColumn(name = "bpTemp_id"))
    private List<Post> scenario;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public DocTemplate getDocTemplate() {
        return docTemplate;
    }

    public void setDocTemplate(DocTemplate docTemplate) {
        this.docTemplate = docTemplate;
    }

    public List<Post> getScenario() {
        return scenario;
    }

    public void setScenario(List<Post> scenario) {
        this.scenario = scenario;
    }
}
