package ru.inno.controller;

import java.util.List;

class ReceivedObject {
    private String description;
    private List<String> idArr;

    public ReceivedObject() { }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setIdArr(List<String> idArr) {
        this.idArr = idArr;
    }

    @Override
    public String toString() {
        return description + " " + idArr;
    }
}
