package ru.inno.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.inno.domain.DocTemplate;
import ru.inno.repos.DocTemplateRepo;

import java.util.Map;

@Controller
public class DocTemplateController {

    @Autowired
    private DocTemplateRepo docTemplateRepo;

    @GetMapping("/getAllDocTemplates")
    public @ResponseBody Iterable<DocTemplate> getAll() {
        return docTemplateRepo.findAll();
    }

    @GetMapping("/allDocTemplates")
    public String getAllDocTemplates(Map<String, Object> model) {
        Iterable<DocTemplate> docTemplates = docTemplateRepo.findAll();
        model.put("docTemplates", docTemplates);
        return "allDocTemplates";
    }

    @PostMapping("/allDocTemplates")
    public String addDocTemplate(@RequestParam String link, Map<String, Object> model) {
        DocTemplate docTemplate = new DocTemplate();
        docTemplate.setLink(link);
        docTemplateRepo.save(docTemplate);

        return getAllDocTemplates(model);
    }
}
