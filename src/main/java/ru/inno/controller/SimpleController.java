package ru.inno.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SimpleController {

    @GetMapping("/tasks_of_bp")
    public String main() {
        return "tasks_of_bp";
    }
}
