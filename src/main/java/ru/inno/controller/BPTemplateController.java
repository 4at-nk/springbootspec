package ru.inno.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.inno.domain.BPTemplate;
import ru.inno.repos.BPTemplateRepo;

import java.util.Map;

@RestController
public class BPTemplateController {

    @Autowired
    private BPTemplateRepo bpTemplateRepo;

    @GetMapping("/allBPTemplates")
    public String getAllBPTemplates(Map<String, Object> model) {
        Iterable<BPTemplate> bpTemplates = bpTemplateRepo.findAll();
        model.put("bpTemplates", bpTemplates);
        return "allBPTemplates";
    }

    @PostMapping(value="/allBPTemplates")
    public String addBPTemplate(@RequestBody ReceivedObject object,
                                Map<String, Object> model) {

        System.out.println(object);

        return getAllBPTemplates(model);
    }
}
