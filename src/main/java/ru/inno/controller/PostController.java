package ru.inno.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.inno.domain.Post;
import ru.inno.repos.PostRepo;

import java.util.Map;

@Controller
public class PostController {

    @Autowired
    private PostRepo postRepo;

    @GetMapping("/allPosts")
    public String getAllPosts(Map<String, Object> model) {
        Iterable<Post> posts = postRepo.findAll();
        model.put("posts", posts);
        return "allPosts";
    }

    @PostMapping("/allPosts")
    public String addPost(@RequestParam String description, Map<String, Object> model) {
        Post post = new Post();
        post.setDescription(description);
        postRepo.save(post);

        return getAllPosts(model);
    }
}
